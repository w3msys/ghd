<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\HotelRequest;
use App\Http\Requests\ManagerRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Hotel;
use App\Manager;
use App\User;

/*
 * @resource HomeController
 *
 * This is the controller for the logged in user
 *
 */

class HomeController extends Controller
{
    /*
     * summary method
     *
     * no parameters required
     *
     */

    
    public function __construct(){
        $this->middleware('auth:api','cors');
    }


    public function summary(){
        return response()->json([
            'hotels'=>Hotel::all(),
            'managers'=>Manager::all()
        ],200);
    }

    /*
     * New hotel post method
     *
     */
    public function newHotel(HotelRequest $request){

        $validation = Validator::make($request->all(),$request->rules());

        if($validation->fails()){
            return response()->json($validation->getMessageBag(),400);
        }

        $hotel = Hotel::create([
            'name'=>$request['name'],
            'location'=>$request['location'],
            'contact'=>$request['contact']
        ]);

        if(!$hotel){
            return response()->json(['Error creating hotel'],500);
        }

        return response()->json(['hotel'=>$hotel],200);

    }

    /*
     * New Manager Post route
     */
    public function newManager(ManagerRequest $request){

        $validation = Validator::make($request->all(),$request->rules());

        if($validation->fails()){
            return response()->json($validation->getMessageBag(),400);
        }

        $hotel_id = null;

        $hotel = Hotel::where('name',$request['hotel'])->first();

        if($hotel){
            $hotel_id = $hotel->id;
        }

        $manager = Manager::create([
            'name'=>$request['name'],
            'contact'=>$request['contact'],
            'hotel_id'=>$hotel_id
        ]);

        if(!$manager){
            return response()->json(['Error Adding Manager'],500);
        }

        return response()->json(['manager'=>$manager],200);

    }


    public function searchSummary(Request $request){

        $key = $request['q'];

    }

    /*
     * Hotels get method
     *
     * No parameters
     */
    public function getHotels(){
        return response()->json(['hotels'=>Hotel::all()],200);
    }

    /*
     * Managers get method
     *
     * No parameters
     *
     */
    public function getManagers(){
        return response()->json(['managers'=>Manager::all()],200);
    }

    /*
     * Get hotel by id method
     *
     * Parameter id=>integer
     */
    public function getHotelById($id){
        $hotel = Hotel::find($id);

        return response()->json(['hotel'=>$hotel],200);
    }
    /*
     * Get manager by id method
     *
     * Parameter id=>integer
     */
    public function getManagerById($id){
        $manager = Manager::find($id);

        return response()->json(['manager'=>$manager],200);
    }

    /*
     * Delete Manager route by id method
     *
     * Parameter id=>integer
     */
    public function deleteManager($id){
        $manager = Manager::find($id);

        if($manager){
            $manager->delete();

            return response()->json(['Delete Successful'],200);
        }
        return response()->json(['Manager not found'],404);
    }

    /*
    * Delete Hotel route by id method
    *
    * Parameter id=>integer
    */
    public function deleteHotel($id){

        $hotel = Hotel::find($id);

        if($hotel){
            $hotel->delete();

            return response()->json(['Delete Successful'],200);
        }
        return response()->json(['Hotel not found'],404);
    }

    /*
     * Update hotel by id method
     *
     */

    public function updateHotel($id,HotelRequest $request){


        $validation = Validator::make($request->all(),$request->rules());

        if($validation->fails()){
            return response()->json($validation->getMessageBag(),400);
        }

        $hotel = Hotel::find($id);

        $hotel->update([
            'name'=>$request['name'],
            'location'=>$request['location'],
            'contact'=>$request['contact']
        ]);

        return response()->json(['hotel'=>$hotel],200);

    }

    /*
    * Update Manager by id method
    *
    */

    public function updateManager($id,ManagerRequest $request){


        $validation = Validator::make($request->all(),$request->rules());

        if($validation->fails()){
            return response()->json($validation->getMessageBag(),400);
        }

        $hotel_id = null;

        $hotel = Hotel::where('name',$request['hotel'])->first();



        if($hotel){

            $hotel_id = $hotel->id;

        }

        $manager = Manager::find($id);

        $manager->update([
            'name'=>$request['name'],
            'contact'=>$request['contact'],
            'hotel_id'=>$hotel_id
        ]);

        return response()->json(['manager'=>$manager],200);

    }





}
