<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Manager;
use App\Hotel;
use Tymon\JWTAuth\Facades\JWTAuth;

/*
 * @resource LoginController
 *
 * This is the Api login controller
 *
 */

class LoginController extends Controller
{


    /*
     * login
     *
     * This is the login method
     *
     */
   public function __construct(){
        $this->middleware('auth:api','cors');
    }


    public function login(LoginRequest $request){

        $validation = Validator::make($request->all(),$request->rules());

        if($validation->fails()){
            return response()->json($validation->getMessageBag(),400);
        }

        $credentials = request(['username', 'password']);

        if ($token = JWTAuth::attempt($credentials)) {


            return $this->respondWithToken($token);

        }

        return response()->json(['error' => 'Unauthorized'], 401);

        /*$user = User::where('username',$request['username'])->first();

        if(!$user){
            return response()->json(['User not found'],404);
        }


        if (!Hash::check($request['password'], $user->first()->password)) {
            return response()->json(['Incorrect Password'], 202);

            return response()->json([
        'user'=>$user->first(),
        'hotels'=>Hotel::all(),
        'managers'=>Manager::all()
        ], 200);
        }*/




    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {

        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'hotels'=>Hotel::all(),
            'managers'=>Manager::all()
        ]);
    }




}
