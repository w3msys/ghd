<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api|cors')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register','Api\Auth\RegisterController@register')->middleware('cors');

// Login route
Route::post('/login','Api\Auth\LoginController@login')->middleware('cors');

//Api Hotels Route Group
Route::group(['prefix'=>'hotels','middleware'=>'auth:api|cors'],function(){

    //Get Route for hotels
    Route::get('/','Api\HomeController@getHotels');
    //Get Route for single hotel by id
    Route::get('/{id}','Api\HomeController@getHotelById');
    //Post Route for new hotel
    Route::post('/new','Api\HomeController@newHotel');
    //Get Route for deleting hotel by id
    Route::get('/{id}/delete','Api\HomeController@deleteHotel');
    //Put Route for updating hotel by id
    Route::put('/{id}/update','Api\HomeController@updateHotel');

});


//Api Managers route group
Route::group(['prefix'=>'managers','middleware'=>'auth:api|cors'],function(){
    //Get Route for managers
    Route::get('/','Api\HomeController@getManagers');
    //Get Route for single manager by id
    Route::get('/{id}','Api\HomeController@getManagerById');
    //Post Route for new manager
    Route::post('/new','Api\HomeController@newManager');
    //Get Route to delete manager
    Route::get('/{id}/delete','Api\HomeController@deleteManager');
    //Put Route to update manager
    Route::put('/{id}/update','Api\HomeController@updateManager');
});

//Get route to summary
Route::get('/summary','Api\HomeController@summary')->middleware('auth:api|cors');
//Any route to search
Route::any('/search/summary','Api\HomeController@searchSummary')->middleware('auth:api|cors');
