---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#general
<!-- START_67f4533ef93c869c35000b433ae4793e -->
## api/summary

> Example request:

```bash
curl -X GET "http://localhost/api/summary" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/summary",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "hotels": [],
    "managers": []
}
```

### HTTP Request
`GET api/summary`

`HEAD api/summary`


<!-- END_67f4533ef93c869c35000b433ae4793e -->

